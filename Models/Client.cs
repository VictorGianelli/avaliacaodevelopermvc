﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvaliacaoDeveloperMVC.Models
{
    public class Client
    {
        public int Id { get; set; } 
        public string Plate { get; set; }
        public string Renavam { get; set; }
        public string OwnerName { get; set; }
        public string OwnerCPF { get; set; }
        public bool isBlocked { get; set; }

    }
}