﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AvaliacaoDeveloperMVC.Startup))]
namespace AvaliacaoDeveloperMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
