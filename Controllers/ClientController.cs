﻿using AvaliacaoDeveloperMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvaliacaoDeveloperMVC.Controllers
{
    public class ClientController : Controller
    {
        UnitOfWork.UnitOfWorkApp _uow;
        public ClientController()
        {
            _uow = new UnitOfWork.UnitOfWorkApp();
        }
        // GET: Client
        public ActionResult Index()
        {
            return View(_uow.ClientRepository.FindAll());
        }

        // GET: Client/Details/5
        public ActionResult Details(int id)
        {
            return View(_uow.ClientRepository.FindBy(id));
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult Create(Client client)
        {
            try
            {
                // TODO: Add insert logic here
                _uow.ClientRepository.Add(client);
                //foreach (HttpPostedFile postedFile in file.InputStream )
                //{
                //    string fileName = Path.GetFileName(postedFile.FileName);
                //    string fileExtention = Path.GetExtension(fileName);
                //    int fileSize = postedFile.ContentLength;

                //    if (fileExtention == ".jpg" || fileExtention == ".jpeg" || fileExtention == ".png")
                //    {

                //        postedFile.SaveAs(Server.MapPath("Upload/" + guid + "/" + fileName));

                //        //capturar a string de conexao
                //        System.Configuration.Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
                //        System.Configuration.ConnectionStringSettings connString;
                //        connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConnectionString"];
                //        //cria um objeto de conexão
                //        SqlConnection con = new SqlConnection();
                //        con.ConnectionString = connString.ToString();
                //        SqlCommand cmd = new SqlCommand();
                //        cmd.Connection = con;
                //        cmd.CommandText = "AtualizarImageId";
                //        cmd.CommandType = CommandType.StoredProcedure;
                //        cmd.Parameters.AddWithValue("nome", fileName);
                //        //cmd.Parameters.AddWithValue("tamanho", fileSize);
                //        cmd.Parameters.AddWithValue("imagePath", "Upload/" + guid + "/" + fileName);
                //        cmd.Parameters.AddWithValue("Id", );

                //        con.Open();
                //        cmd.ExecuteNonQuery();
                //        con.Close();

                //        //lMsg.Text = "Os dados e imagens foram enviadas com sucesso";

                //    }
                //    else
                //    {
                //        throw new Exception("Formato " + fileExtention + " não é suportado");
                //    }
                //}



                _uow.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //public void Upload (HttpPostedFile file, string guid)
        //{
        //    try
        //    {
        //        if (file.ContentLength > 0)
        //        {
        //            string _FileName = Path.GetFileName(file.FileName);
        //            string _path = Path.Combine(Server.MapPath("~/Upload/" + guid + "/" + fileName"), _FileName);
        //            file.SaveAs(_path);
        //        }
        //        //ViewBag.Message = "File Uploaded Successfully!!";
        //        //return View();
        //    }
        //    catch
        //    {
        //        //ViewBag.Message = "File upload failed!!";
        //        //return View();
        //    }
        //}
    

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_uow.ClientRepository.FindBy(id));
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Client client)
        {
            try
            {
                // TODO: Add update logic here
                _uow.ClientRepository.Edit(client);
                _uow.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_uow.ClientRepository.FindBy(id));
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Client client)
        {
            try
            {
                // TODO: Add delete logic here
                _uow.ClientRepository.Remove(_uow.ClientRepository.FindBy(id));
                _uow.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
