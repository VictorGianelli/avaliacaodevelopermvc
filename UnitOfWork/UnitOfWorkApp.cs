﻿using AvaliacaoDeveloperMVC.Data;
using AvaliacaoDeveloperMVC.Models;
using AvaliacaoDeveloperMVC.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AvaliacaoDeveloperMVC.UnitOfWork
{
    public class UnitOfWorkApp : DbContext
    {
        ContextApp context = new ContextApp();
        Repository<Client> clientRepository;
        public Repository<Client> ClientRepository
        {
            get
            { 
                if (clientRepository == null)
                {
                    clientRepository = new Repository<Client>(context);
                }
                return clientRepository;
            }
        }
        public void Commit()
        {
            context.SaveChanges();
        }
        
    }
}